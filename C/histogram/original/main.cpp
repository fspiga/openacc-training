#include <cstdio>
#include <cstdlib>
using namespace std;

int main() {
  int N=100000000;
  int HN=10000;
  int ITERS=100;

  int *a, *h;
  a=(int*)malloc(N*sizeof(unsigned int));
  h=(int*)malloc(HN*sizeof(unsigned int));


  for(int i=0;i<N;i++)
    a[i]=lrand48()%HN;;


  for(int it=0;it<ITERS;it++) {
  
    for(int i=0;i<HN;i++)
      h[i]=0;
    for(int i=0;i<N;i++) {
      h[a[i]]++;
    }
  }

  return 0;
}

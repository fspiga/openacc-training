#include <cstdio>
#include <cstdlib>
using namespace std;


int main() {
  int N=100000000;
  int HN=10000;
  int ITERS=100;
  
  int *a, *h;
  a=(int*)malloc(N*sizeof(int));
  h=(int*)malloc(HN*sizeof(int));


  for(int i=0;i<N;i++)
    a[i]=lrand48()%HN;

  #pragma acc data copyin(a[0:N]) copyout(h[0:HN])
  for(int it=0;it<ITERS;it++)
  {
    #pragma acc parallel loop
    for(int i=0;i<HN;i++) 
      h[i]=0;
    
    #pragma acc parallel loop
    for(int i=0;i<N;i++) {
      #pragma acc atomic
      h[a[i]]+=1;
    }
  }

  return 0;
}

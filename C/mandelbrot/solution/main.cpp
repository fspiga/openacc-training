#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <time.h>
#include "mandlebrot.h"
using namespace std;

float myclock() {
    timespec t;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
    return (float)t.tv_sec+t.tv_nsec/(float)1e9; 
}

int main() {
  float start, end;

  size_t bytes=WIDTH*HEIGHT*sizeof(unsigned char);
  unsigned char *image=(unsigned char*)malloc(bytes);
  FILE *fp=fopen("image.pgm","wb");
  fprintf(fp,"P5\n%s\n%d %d\n%d\n","#comment",WIDTH,HEIGHT,MAX_COLOR);

  unsigned int BATCH_SIZE=8;
  unsigned int NUM_BATCHES=HEIGHT/BATCH_SIZE;

  start=myclock();
  #pragma acc data create(image[0:WIDTH*HEIGHT])
  {
    for(unsigned int yp=0;yp<NUM_BATCHES;yp++) {
      unsigned int ystart=yp*BATCH_SIZE;
      unsigned int yend=ystart+BATCH_SIZE;
      #pragma acc parallel loop collapse(2) async(yp%2)
      for(unsigned int y=ystart;y<yend;y++) {
        for(unsigned int x=0;x<WIDTH;x++) {
          image[y*WIDTH+x]=mandlebrot(x,y);
        }
      }
      #pragma acc update host(image[yp*BATCH_SIZE*WIDTH:WIDTH*BATCH_SIZE]) async(yp%2)
    }
  }
  #pragma acc wait
  end=myclock();
  printf("Mandlebrot time: %f seconds\n", end-start);
  
  fwrite(image,sizeof(unsigned char),WIDTH*HEIGHT,fp);
  fclose(fp);
  free(image);
  return 0;
}

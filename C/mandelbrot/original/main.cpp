#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <time.h>
#include "mandlebrot.h"
using namespace std;

float myclock() {
    timespec t;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
    return (float)t.tv_sec+t.tv_nsec/(float)1e9; 
}

int main() {
  float start,end;
 
  size_t bytes=WIDTH*HEIGHT*sizeof(unsigned char);
  unsigned char *image=(unsigned char*)malloc(bytes);
  FILE *fp=fopen("image.pgm","wb");
  fprintf(fp,"P5\n%s\n%d %d\n%d\n","#comment",WIDTH,HEIGHT,MAX_COLOR);
  start=myclock();
  for(int y=0;y<HEIGHT;y++) {
    for(int x=0;x<WIDTH;x++) {
      image[y*WIDTH+x]=mandlebrot(x,y);
    }
  }
  end=myclock();
  printf("Mandlebrot time: %f seconds\n", end-start);
  
  fwrite(image,sizeof(unsigned char),WIDTH*HEIGHT,fp);
  fclose(fp);
  free(image);
  return 0;
}

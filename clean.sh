#!/bin/bash
FILES=`find . -name Makefile`

for file in $FILES; do
  DIR=`dirname $file`
  make -C $DIR clean
done

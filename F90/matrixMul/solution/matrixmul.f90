module mymm
contains
    subroutine mm1( a, b, c, m )
     real, dimension(:,:) :: a,b,c
!$acc region
     do j = 1,m
         do i = 1,m
          a(i,j) = 0.0
         enddo
       do k = 1,m
         do i = 1,m
          a(i,j) = a(i,j) + b(i,k) * c(k,j)
         enddo
       enddo
     enddo
!$acc end region
    end subroutine
end module

integer function ival( arg )
     character*(*) :: arg
     character*1 :: ch
     ival = 0
     do i = 1, len(arg)
      ch = arg(i:i)
      if( ch .ge. '0' .and. ch .le. '9' )then
       ival = ival * 10 + ichar(ch) - ichar('0')
      endif
     enddo
end function

program p 
     use mymm

     real, allocatable :: a(:,:), b(:,:), c(:,:)
     integer t1, t2, nn, iters
    
     ! Change me - matrix size
     nn = 4096
  
     ! Change me - number of iterations
     iters = 1
     
     allocate(a(nn,nn))
     allocate(b(nn,nn))
     allocate(c(nn,nn))
     
     do j = 1,nn
       do i = 1,nn
        b(i,j) = i*1000 + j
        c(i,j) = i+j
        a(i,j) = -1
       enddo
      enddo


     do itime = 1,iters
      call cpu_time(r1);
      call mm1( a, b, c, nn )
      call cpu_time(r2);
      rmsec = 1000.*(r2-r1)
      print *, "> cpu_time milliseconds = ", rmsec
    enddo

end program


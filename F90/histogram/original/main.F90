program example4
  implicit none
  integer, parameter :: N=10000000, HN=10000, ITERS=100
  integer            :: a(N), h(HN), i, it
  real               :: tmp


  do i=1,N
    call random_number(tmp)
    a(i) = tmp * HN
  enddo

  do it=1,ITERS
    h(:) = 0
    do i=1,N
      h(a(i)) = h(a(i)) + 1
    enddo
  enddo

  print *, h(1:5)

end program example4

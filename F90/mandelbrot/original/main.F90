program example3
use mod_example3
implicit none
integer, parameter   :: BATCH_SIZE=8
integer(1) :: image(HEIGHT, WIDTH)
integer :: iy, ix
real :: startt, stopt
image = 0

call cpu_time(startt)
do iy=1,width
  do ix=1,HEIGHT
    image(ix,iy) = min(max(int(mandlebrot(ix-1,iy-1)),0),MAXCOLORS)
  enddo
enddo
call cpu_time(stopt)

print *,"Time:",(stopt-startt)

call write_pgm(image,'image.pgm')
end

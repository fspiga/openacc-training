#!/bin/bash
#SBATCH --job-name=OpenACC
#SBATCH --account=TRAINING-GPU
#SBATCH --nodes=1
#SBATCH --time=00:05:00
#SBATCH --no-requeue
#SBATCH --partition=pascal
#SBATCH --gres=gpu:1
#SBATCH --reservation=openacc

### DO NOT CHANGE THESE ###
. /etc/profile.d/modules.sh
module purge
module load rhel7/default-gpu
module load pgi/2018
##########################

# Name of your executable here
hostname

